In command prompt/terminal, go to your desired instalation directory and:

###### Window
```
git clone ssh://git@stash.emag.network/emgctf/front-end-starter.git;
rename front-end-starter yourProjectName;
cd yourProjectName;
yarn install/npm install;
```

###### Linux
```
git clone ssh://git@stash.emag.network/emgctf/front-end-starter.git;
mv front-end-starter yourProjectName;
cd yourProjectName;
yarn install/npm install;
```